import React, { useState } from "react";

import DayVisitsReport from "./dayVisitsReport";
import CountriesReport from "./countriesReport";
import PageviewsReport from "./pageViewReport";
import SourceReport from "./sourceReport";
import BrowsersReport from "./browsersReport";
import Header from "../components/header";
import InputField from "../components/input";

const DashBoard = () => {
  const [viewID, setViewID] = useState(null);

  return (
    <>
      <Header />
      {viewID ? (
        <>
          <hr />
          <PageviewsReport viewID={viewID} />
          <DayVisitsReport
            metric={"ga:users"}
            title={"Users"}
            viewID={viewID}
          />
          <DayVisitsReport
            metric={"ga:sessions"}
            title={"Sessions"}
            viewID={viewID}
          />
          <CountriesReport viewID={viewID} />
          <SourceReport viewID={viewID} />
          <BrowsersReport viewID={viewID} />
        </>
      ) : (
        <InputField submitViewId={(id) => setViewID(id)} />
      )}
    </>
  );
};

export default DashBoard;
